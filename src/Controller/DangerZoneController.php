<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Zones;
use App\Repository\ZonesRepository;
use Symfony\Component\HttpFoundation\Request;

use Doctrine\ORM\EntityManagerInterface;


class DangerZoneController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Zones::class);

        $zones = $repo->findAll();

        return $this->render('danger_zone/index.html.twig', [
            'controller_name' => 'DangerZoneController',
            'zones' => $zones,
        ]);
    }

    /**
     * @Route("/create", name="create_zone")
     * @Route("/edit/{id}", name="edit_zone")
     */
    public function form(Zones $zone = null , Request $request, EntityManagerInterface $manager)
    {
        if(!$zone) {
            $zone = new Zones();
        }

        $user = $this->getUser();

        $form = $this->createFormBuilder($zone)
                     ->add('name')
                     ->add('resume')
                     ->add('gps')
                     ->add('ore')
                     ->add('danger')
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            if(!$zone->getId()) {
                $zone->setCreatedAt(new \DateTime());
            }

            $zone->setScientist($user);

            $manager->persist($zone);
            $manager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('danger_zone/create.html.twig', [
            'formZone' => $form->createView(),
            'editMode' => $zone->getId() !== null
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function remove(ZonesRepository $repo, $id, EntityManagerInterface $manager)
    {
        $identifiant = $repo->find($id);

        $manager->remove($identifiant);
        $manager->flush($identifiant);

        return $this->redirectToRoute('home');

    }
}
