<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ZonesRepository")
 */
class Zones
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resume;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gps;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ore;

    /**
     * @ORM\Column(type="integer")
     */
    private $danger;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="zones")
     * @ORM\JoinColumn(nullable=false)
     */
    private $scientist;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(?string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getGps(): ?string
    {
        return $this->gps;
    }

    public function setGps(string $gps): self
    {
        $this->gps = $gps;

        return $this;
    }

    public function getOre(): ?string
    {
        return $this->ore;
    }

    public function setOre(string $ore): self
    {
        $this->ore = $ore;

        return $this;
    }

    public function getDanger(): ?int
    {
        return $this->danger;
    }

    public function setDanger(int $danger): self
    {
        $this->danger = $danger;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getScientist(): ?User
    {
        return $this->scientist;
    }

    public function setScientist(?User $scientist): self
    {
        $this->scientist = $scientist;

        return $this;
    }
}
